using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StatDisplay : MonoBehaviour 
{
    [SerializeField] private TextMeshProUGUI _statName;
    [SerializeField] private TextMeshProUGUI _statValue;
    [SerializeField] private TextMeshProUGUI _statPreview;

    public void SetStatName( string name )
    {
        _statName.text = name;
    }

    public void SetStatValue( string value )
    {
        _statValue.text = value;
    }

    public void EnableStatPreview( string previewValue )
    {
        _statPreview.text = $"=> {previewValue}";
    }
    public void DisableStatPreview()
    {
        _statPreview.enabled = false;
    }
}
