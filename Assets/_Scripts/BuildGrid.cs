using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BuildGrid : MonoBehaviour
{
    public bool gizmos = true;
    private float _buildingPlaneYOffset = 0.05f;

    private Tile[,] _tileArray;
    public Tile[,] TileArray => _tileArray;

    private Structure[,] _structureArray;
    public Structure[,] StructureArray => _structureArray;

    private GameObject _buildingPlane;



    /// <summary> Initialize an empty grid array with given dimensions </summary>
    public void Init( Vector2Int gridDimensions )
    {
        _tileArray = new Tile[ gridDimensions.x, gridDimensions.y ];
        _structureArray = new Structure[ gridDimensions.x, gridDimensions.y ];
        InitializeBuildGrid();
    }

    /// <summary> Initialize a grid array from saved data </summary>
    public void Init( LevelTileDataSO levelTileData )
    {
        _tileArray = new Tile[ levelTileData.tileIdArray.GetLength( 0 ), levelTileData.tileIdArray.GetLength( 1 ) ];

        for ( int i = 0; i < _tileArray.GetLength( 0 ); i++ )
        {
            for ( int j = 0; j < _tileArray.GetLength( 1 ); j++ )
            {
                Tile tile = TileFactory.InstantiateTileFromId( levelTileData.tileIdArray[ i, j ] ).GetComponent<Tile>();
                if ( tile.IsTransparent )
                {
                    //tile.transform.SetPositionAndRotation( new Vector3();

                }

                // get water tile rotation
                // AddTileToGrid( new Vector2Int( i, j ), Instantiate( tilePrefab, new Vector3( i + 0.5f, j + 0.5f, transform.position.z ) );

                //_tileArray[ i, j ] = 
            }
        }
        _structureArray = new Structure[ levelTileData.tileIdArray.GetLength( 0 ), levelTileData.tileIdArray.GetLength( 1 ) ];
    }

    public void InitializeBuildGrid()
    {
        if ( _buildingPlane != null ) { Destroy( _buildingPlane ); }

        _buildingPlane = GameObject.CreatePrimitive( PrimitiveType.Plane );
        _buildingPlane.transform.SetParent( transform, false );
        _buildingPlane.transform.position += Vector3.up * _buildingPlaneYOffset;
        _buildingPlane.GetComponent<MeshRenderer>().material = Resources.Load( "Materials/mat_gridLines_001" ) as Material;
        _buildingPlane.layer = LayerMask.NameToLayer( "BuildingGrid" );

        Vector2Int offset = new Vector2Int();
        if ( _tileArray.GetLength( 0 ) % 2 != 0 )
        {
            offset.x += 1;
        }
        if ( _tileArray.GetLength( 1 ) % 2 != 0 )
        {
            offset.y += 1;
        }
        SetBuildingPlaneTransform( offset.x, offset.y );
    }

    /// <param name="relativeOffsetX">optional offset param</param>
    /// <param name="relativeOffsetY">optional offset param</param>
    public void SetBuildingPlaneTransform( int relativeOffsetX = default( int ), int relativeOffsetY = default( int ) )
    {
        // Scale to array size. * 0.1f because plane primitives are 10 by 10 by default
        _buildingPlane.transform.localScale = new Vector3( _tileArray.GetLength( 0 ) * 0.1f, 1f, _tileArray.GetLength( 1 ) * 0.1f );

        if ( relativeOffsetX != 0 || relativeOffsetY != 0 )
        {
            _buildingPlane.transform.position = new Vector3(
                _buildingPlane.transform.position.x + relativeOffsetX * 0.5f,
                _buildingPlane.transform.position.y,
                _buildingPlane.transform.position.z + relativeOffsetY * 0.5f
                );
        }
    }

    public void AddTileToGrid( Vector2Int index, Tile tile )
    {
        _tileArray[ index.x, index.y ] = tile;

    }

    public void RemoveTileFromGrid( Vector2Int index )
    {
        _tileArray[ index.x, index.y ] = null;
    }

    public void TrimEmptyGridSpace()
    {
        // first value of the vector represents bottom of array, second value the top
        Vector2Int xTrim = new Vector2Int( 0, 0 );
        Vector2Int yTrim = new Vector2Int( 0, 0 );

        // trimming bottom of array 
        bool xDone = false, yDone = false;
        Vector2Int upperBounds = new Vector2Int( _tileArray.GetUpperBound( 0 ), _tileArray.GetUpperBound( 1 ) );

        int offset = 0;
        while ( !xDone || !yDone )
        {
            if ( offset < _tileArray.GetLength( 1 ) )
            {
                for ( int i = xTrim.x; i <= upperBounds.x; i++ )
                {
                    if ( _tileArray[ i, offset ] != null )
                    {
                        xDone = true;
                        break;
                    }
                }
                if ( !xDone ) { yTrim.x++; }
            }
            else { xDone = true; }

            if ( offset < _tileArray.GetLength( 0 ) )
            {
                for ( int i = yTrim.x; i <= upperBounds.y; i++ )
                {
                    if ( _tileArray[ offset, i ] != null )
                    {
                        yDone = true;
                        break;
                    }
                }
                if ( !yDone ) { xTrim.x++; }
            }
            else { yDone = true; }

            offset++;
        }

        // trim top
        offset = 0;
        xDone = false;
        yDone = false;
        while ( !xDone || !yDone )
        {
            if ( offset < _tileArray.GetLength( 1 ) - yTrim.x )
            {
                for ( int i = upperBounds.x; i >= xTrim.x; i-- )
                {
                    if ( _tileArray[ i, upperBounds.y - offset ] != null )
                    {
                        xDone = true;
                        break;
                    }
                }
                if ( !xDone )
                {
                    yTrim.y++;
                }
            }
            else
            {
                xDone = true;
            }

            if ( offset < _tileArray.GetLength( 0 ) - xTrim.x )
            {
                for ( int i = upperBounds.y; i >= yTrim.x; i-- )
                {
                    if ( _tileArray[ upperBounds.x - offset, i ] != null )
                    {
                        yDone = true;
                        break;
                    }
                }
                if ( !yDone )
                {
                    xTrim.y++;
                }
            }
            else
            {
                yDone = true;
            }

            offset++;
        }

        ResizeTileGrid( xTrim.x * -1, xTrim.y * -1, yTrim.x * -1, yTrim.y * -1 );
    }

    public void ResizeTileGrid( int xLowerResize, int xUpperResize, int yLowerResize, int yUpperResize )
    {
        int originalRows = _tileArray.GetLength( 0 );
        int originalCols = _tileArray.GetLength( 1 );
        int newRows = originalRows + xLowerResize + xUpperResize;
        int newCols = originalCols + yLowerResize + yUpperResize;

        if ( newRows <= 0 || newCols <= 0 )
        {
            throw new ArgumentException( "Invalid bounds, array dimensions must be at least 1,1" );
        }

        Tile[,] newArray = new Tile[ newRows, newCols ];
        int startRow = Math.Max( 0, -xLowerResize );
        int startCol = Math.Max( 0, -yLowerResize );
        int copyStartRow = Math.Max( 0, xLowerResize );
        int copyStartCol = Math.Max( 0, yLowerResize );
        int copyRows = Math.Min( newRows, originalRows );
        int copyCols = Math.Min( newCols, originalCols );

        for ( int i = 0; i < copyRows; i++ )
        {
            Array.Copy( _tileArray, (i + startRow) * originalCols + startCol, newArray, (i + copyStartRow) * newCols + copyStartCol, copyCols );
        }
        _tileArray = newArray;
        SetBuildingPlaneTransform( xUpperResize - xLowerResize, yUpperResize - yLowerResize );
    }

    public static List<Tile> GetGoalTiles( Tile[,] tiles )
    {
        List<Tile> goals = new List<Tile>();
        for ( int i = 0; i < tiles.GetLength( 0 ); i++ )
        {
            for ( int j = 0; j < tiles.GetLength( 1 ); j++ )
            {
                if ( tiles[ i, j ].TileType == TileType.Goal )
                {
                    goals.Add( tiles[ i, j ] );
                }
            }
        }
        return goals;
    }
    public static List<Tile> GetSpawnerTiles( Tile[,] tiles )
    {
        List<Tile> spawners = new List<Tile>();
        for ( int i = 0; i < tiles.GetLength( 0 ); i++ )
        {
            for ( int j = 0; j < tiles.GetLength( 1 ); j++ )
            {
                if ( tiles[ i, j ].TileType == TileType.Spawner )
                {
                    spawners.Add( tiles[ i, j ] );
                }
            }
        }
        return spawners;
    }

    public Vector2Int WorldPosToGridCoords( Vector3 worldPos )
    {
        return new Vector2Int(
            Mathf.FloorToInt( worldPos.x + _tileArray.GetLength( 0 ) * 0.5f - _buildingPlane.transform.localPosition.x ),
            Mathf.FloorToInt( worldPos.z + _tileArray.GetLength( 1 ) * 0.5f - _buildingPlane.transform.localPosition.z )
        );
    }
    /// <summary>Returns the local position of the center of a cell at specified grid coordinates, relative to the building grid's position </summary>
    public Vector3 GridCoordsToLocalCellCenter( Vector2Int coords )
    {
        return new Vector3(
            coords.x + _buildingPlane.transform.localPosition.x - _tileArray.GetLength( 0 ) * 0.5f + 0.5f,
            transform.position.y,
            coords.y + _buildingPlane.transform.localPosition.z - _tileArray.GetLength( 1 ) * 0.5f + 0.5f
            );
    }

    void OnDrawGizmos()
    {
        if ( gizmos && _tileArray != null )
        {
            for ( int i = 0; i <= _tileArray.GetUpperBound( 0 ); i++ )
            {
                for ( int j = 0; j <= _tileArray.GetUpperBound( 1 ); j++ )
                {
                    Vector3 center = new Vector3(
                        transform.position.x - _tileArray.GetUpperBound( 0 ) * 0.5f + i,
                        transform.position.y,
                        transform.position.z - _tileArray.GetUpperBound( 1 ) * 0.5f + j
                    );

                    if ( _tileArray[ i, j ] == null )
                    {
                        Gizmos.color = Color.black;
                        Gizmos.DrawWireCube( center, Vector3.one * 0.4f );
                    }
                    else
                    {
                        Gizmos.color = Color.red;
                        Gizmos.DrawCube( center, Vector3.one * 0.35f );
                    }
                }
            }
        }
    }
}