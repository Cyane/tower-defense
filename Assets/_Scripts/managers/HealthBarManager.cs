using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Canvas))]
public class HealthBarManager : MonoBehaviour
{
    /*
    public HealthBar healthBarPrefab;
    public List<HealthBar> _healthBars = new List<HealthBar>();
    private Camera _mainCam;
    private Canvas _canvas;
    public float canvasScaling => _canvas.scaleFactor;

    public static HealthBarManager instance { get; private set; }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }
    private void Start()
    {
        _mainCam = Camera.main;
        _canvas = GetComponent<Canvas>();
    }

    public void SpawnHealthBar( Enemy enemy )
    {
        HealthBar healthBar = Instantiate( healthBarPrefab, transform );
        //healthBar.InitHealthBar( enemy );
        healthBar.OnDelete += RemoveFromList;
        _healthBars.Add( healthBar );
    }

    private void RemoveFromList( HealthBar hpBar )
    {
        _healthBars.Remove( hpBar );
    }

    private void LateUpdate()
    {
        foreach( HealthBar hpBar in _healthBars )
        {
            Vector2 newPos = _mainCam.WorldToScreenPoint( hpBar._enemy.transform.position ) / _canvas.scaleFactor;
            hpBar.UpdateHealthBar( newPos );
        }
    }
    */
}
