using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingManager : MonoBehaviour
{
    [SerializeField] private GameObject _selectedObject;
    [SerializeField] private Material _previewMaterial;
    [SerializeField] private Color _validColor, _invalidColor;
    private GameObject _previewObject;
    private bool _buildModeActive;
    
    private bool _canAffordItem;
    
    [SerializeField] private LayerMask  _buildGridMask, _tileMask;

    public static BuildingManager Instance {get; private set;}

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
    
    void Update()
    {
        if (_buildModeActive)
        {
            PreviewTilesOnGrid();
            PlaceSelectedTile();
        }
    }

    public void ToggleBuildMode(bool active)
    {
        _buildModeActive = active;
    }
    
    public void SetSelectedObject(GameObject selectedObject)
    {
        GameManager.instance.GoldAmountChanged -= CheckItemAffordability;
        _selectedObject = selectedObject;
        if ( _previewObject != null ) { Destroy( _previewObject ); }
        _previewObject = Instantiate(_selectedObject);

        foreach (Collider col in _previewObject.GetComponentsInChildren<Collider>())
        {
            col.enabled = false;
        }

        if(selectedObject.TryGetComponent(out Structure structure))
        {
            GameManager.instance.GoldAmountChanged += CheckItemAffordability;
            foreach (MeshRenderer renderer in _previewObject.GetComponentsInChildren<MeshRenderer>())
            {
                renderer.material = _previewMaterial;
            }
        }
        _previewObject.SetActive(false);
    }

    void PreviewStructuresOnGrid()
    {
        if (_previewObject != null)
        {
            
        }
    }

    void PreviewTilesOnGrid()
    {
        if (_previewObject != null)
        {
            if (RaycastMousePosition(out RaycastHit hit, _tileMask))
            {
                if (!_previewObject.activeSelf) { 
                    _previewObject.SetActive(true); 
                }
                _previewObject.transform.position = new Vector3Int( (int)Mathf.Round( hit.point.x ), 0, (int)Mathf.Round( hit.point.z ) );
            }
            else if (_previewObject.activeSelf)
            {
                _previewObject.SetActive(false);
            }
        }
    }

    void PlaceSelectedTile()
    {
        if (Input.GetButton("Fire1") && _previewObject.activeSelf && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        {
            Instantiate(_selectedObject, _previewObject.transform.position, Quaternion.identity);
        }

        if (Input.GetButton("Fire2") && RaycastMousePosition(out RaycastHit hit, _tileMask) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        {
            Destroy(hit.transform);
        }
    }

    void CheckItemAffordability()
    {

    }

    bool RaycastMousePosition(out RaycastHit hit, LayerMask layerMask = default(LayerMask))
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        return Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask);
    }
}
