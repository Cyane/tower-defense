using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("Prefabs")]
    public List<Tile> tilePrefabs;
    [SerializeField] private List<GameObject> structurePrefabs;
    [SerializeField] private StructureButton _structureButtonPrefab;
    [SerializeField] private Button _mainMenuButtonPrefab;
    
    [Header("References")]
    [SerializeField] private GameObject _buildPanel;
    [SerializeField] private GameObject _buildButtonPanel;
    [SerializeField] private GameObject _buildButton;
    [SerializeField] private GameObject _mainMenuPanel;
    [SerializeField] private GameObject _mainMenuButtonPanel;

    public static UIManager instance { get; private set; }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }


    private void Start()
    {
        //InstantiateItemButtons(structurePrefabs);
    }

    public void ShowBuildPanel()
    {
        _buildPanel.SetActive(true);
        _buildButton.SetActive(false);
        BuildingManager.Instance.ToggleBuildMode(true);
    }

    public void InstantiateItemButtons(List<GameObject> structures)
    {
        foreach (Transform child in _buildButtonPanel.transform) { Destroy(child.gameObject); } // Destroy old buttons. We want FRESH buttons

        foreach (GameObject structure in structures)
        {
            GameObject buttonInstance = Instantiate(_structureButtonPrefab.gameObject, _buildButtonPanel.transform);
            StructureButton button = buttonInstance.GetComponent<StructureButton>();
            button.Init(structure);
        }
        /*
        for (int i = 0; i < structures.Count; i++)
        {
            int index = i; // needed because the addlistener delegate otherwise takes the variable reference instead of the value

            GameObject buttonInstance = Instantiate(_structureButtonPrefab.gameObject, _buildButtonPanel.transform);
            StructureButton button = buttonInstance.GetComponent<StructureButton>();
            button.Init(structures[index]);
        }
        */
    }

    public void HideBuildPanel()
    {
        _buildPanel.SetActive(false);
        _buildButton.SetActive(true);
        BuildingManager.Instance.ToggleBuildMode(false);
    }

    public void ShowBuildButton()
    {
        _buildButton.SetActive(true);
    }

    public void HideBuildButton()
    {
        _buildButton.SetActive(false);
    }

    public void ToggleMainMenu(bool activate)
    {
        _mainMenuPanel.SetActive(activate);
    }
}
