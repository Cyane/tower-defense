using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    GameObject enemyGroundPrefab;
    List<Enemy> activeEnemies = new List<Enemy> ();


    public void SpawnEnemy( EnemySpawnArgs args )
    {
        GameObject enemy = Instantiate(enemyGroundPrefab);
        enemy.transform.position = args.spawnPosition;
    }
}

public class EnemySpawnArgs
{
    public Vector3 spawnPosition;

    public EnemySpawnArgs(Vector3 position)
    {
        spawnPosition = position;

    }
    
}
