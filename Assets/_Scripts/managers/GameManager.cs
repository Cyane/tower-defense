using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public UIManager menuManager;
    public BuildingManager buildingManager;
    public LevelManager levelManager;
    public EnemyManager enemyManager;
    // public HealthBarManager healthBarManager; // Enemy UI is simplified with worldspace canvasses

    // learn more about delegates, actions and events
    // https://discussions.unity.com/t/delegate-action-passing-a-reference-of-the-gameobject-that-trigger-the-event/717554/3
    // https://unity.com/how-to/create-modular-and-maintainable-code-observer-pattern
    // https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/event
    // Scriptable obkjects
    // https://learn.unity.com/tutorial/introduction-to-scriptable-objects
    // https://www.youtube.com/watch?v=9gscwiS3xsU
    // Saving to a file and encoding
    // https://discussions.unity.com/t/base64-encode-decoding/12507
    // https://docs.unity3d.com/2022.3/Documentation/ScriptReference/PlayerPrefs.html
    // 
    // Collision/trigger from child objects (for trigger cone)
    // https://gamedev.stackexchange.com/questions/151670/how-to-detect-collision-occurring-on-a-child-object-from-a-parent-script


    private float _gold = 0f;
    public float Gold { get { return _gold; }  }
    
    public event Action GoldAmountChanged;
    public StateMachine stateMachine {get; private set;}
    public static GameManager instance { get; private set; }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    void Start()
    {
        if (menuManager == null) { FindFirstObjectByType<UIManager>(FindObjectsInactive.Include); }
        if (buildingManager == null) { FindFirstObjectByType<BuildingManager>(FindObjectsInactive.Include); }
        if (levelManager == null) { FindFirstObjectByType<LevelManager>(FindObjectsInactive.Include); }
        if (enemyManager == null) { FindFirstObjectByType<EnemyManager>(FindObjectsInactive.Include); }
        //if (healthBarManager == null) { FindFirstObjectByType<HealthBarManager>(FindObjectsInactive.Include); }
        stateMachine = new StateMachine(gameObject);
    }

    void Update()
    {
        //stateMachine.OnUpdate();
    }
    
    void FixedUpdate()
    {
        //stateMachine.OnFixedUpdate();
    }

    /// <summary>Use a negative value to subtract gold.</summary>
    public void AdjustGold(float amount)
    {
        _gold += amount;
        GoldAmountChanged?.Invoke();
    }
}

namespace GameStates
{
    public class MainMenu : State
    {
        public override void OnExitState()
        {
            GameManager.instance.menuManager.ToggleMainMenu(false);
        }
    }
    //public class 
}


