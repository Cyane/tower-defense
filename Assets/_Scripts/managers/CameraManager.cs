using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CameraManager : Singleton<CameraManager>
{
    public Camera MainCam => GetMainCam();
    private Camera _mainCam;
    public Camera UiCam { get; private set; }
    public bool IsOrthogonal => MainCam.orthographic;

    public float zoomEasing = 0.75f;
    public float movementEasing = 0.75f;

    private float _targetZoom;
    private Vector2 _targetPos;

    protected override void Awake()
    {
        base.Awake();
        UiCam = MainCam.transform.Find("UI Camera").GetComponent<Camera>();
        
        if (UiCam == null) 
        {
            if (!GameObject.FindGameObjectWithTag("UICamera"))
            {
                Debug.LogError("There is no UI camera present in the scene.");
            }
        }
    }

    void FixedUpdate()
    {
        UpdateZoom();
        UpdatePosition();
    }

    private Camera GetMainCam()
    {
        if (_mainCam == null )
        {
            _mainCam = Camera.main;
        }
        return _mainCam;
    }

    public void AdjustCameraZoom(float amount)
    {
        _targetZoom += amount;
    } 
    
    public void MoveCameraPosition(Vector2 offset)
    {
        _targetPos += offset;
    }

    private void UpdateZoom()
    {
        //todo: lerp current to target for both cameras, set to target if within small margin
    }

    private void UpdatePosition()
    {
        //todo: lerp current to target, set to target if within small margin
    }


}
