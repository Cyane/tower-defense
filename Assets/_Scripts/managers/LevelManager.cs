using System.Collections;
using System.Collections.Generic;
using Unity.AI.Navigation;
using UnityEngine;
using UnityEngine.AI;

public class LevelManager : MonoBehaviour
{
    public NavMeshSurface groundSurface;
    public NavMeshSurface aerialSurface;

    //sla geplaatste torens op in een 2D array
    /* manages:
     * gold
     * tile grid
     * tower grid
     * navmeshes
     * 
    */

    void Start()
    {
        
    }

    public void RebuildGroundNavMesh()
    {
        groundSurface.BuildNavMesh();
    }
    
    public void RebuildAerialNavMesh()
    {
        aerialSurface.BuildNavMesh();
    }
}