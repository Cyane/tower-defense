using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingMissile : Explosive
{
    //public Enemy _target;
    [SerializeField] private GameObject _smokeTrail;
    [SerializeField] private float _rotationForce;
    [SerializeField] private float _steeringThreshold;
    [SerializeField] private float _propulsionForce;
    [SerializeField, Tooltip( "In meters per second" )] private float _maxSpeed;
    [SerializeField, Tooltip( "in degrees per second" )] private float _maxRotationSpeed;
    [SerializeField] private bool _itanoWobble;
    [SerializeField] private float _itanoWobbleIntensity;
    [SerializeField] private bool _tracking;
    [SerializeField] private bool _gizmos;
    private Vector3 _targetOffset;

    protected override void Start()
    {
        base.Start();
        if ( _itanoWobble ) { SetNewRandomTargetOffset(); }
        _rb.maxAngularVelocity = _maxRotationSpeed * Mathf.Deg2Rad;
        _rb.maxLinearVelocity = _maxSpeed;
    }


    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        if ( _tracking )
        {
            if ( _target != null )
            {
                HomeInOnTarget();
            }
            else
            {
                StopTracking();

                //JustGoStraight();
            }
        }
    }

    protected override void OnEndOfLifetime()
    {
        StopTracking();
    }

    private void HomeInOnTarget()
    {
        Vector3 targetDirection = _target.transform.position - transform.position + _targetOffset;

        float angleInDegrees;
        Vector3 rotationAxis;
        Quaternion.FromToRotation( transform.forward, targetDirection ).ToAngleAxis( out angleInDegrees, out rotationAxis );


        ///todo:
        /// rotate towards target + offset
        /// if rb.velocity points towards target + offset, change offset position

        if ( angleInDegrees > _steeringThreshold )
        {
            _rb.AddTorque( rotationAxis * _rotationForce, ForceMode.VelocityChange );
        }
        _rb.AddForce( transform.forward * _propulsionForce, ForceMode.VelocityChange );

        if ( _itanoWobble && Vector3.Angle( _rb.velocity, targetDirection ) < 1f )
        {
            SetNewRandomTargetOffset();
        }
    }

    public void StopTracking()
    {
        _tracking = false;
        _rb.useGravity = true;
    }

    public void SetNewRandomTargetOffset()
    {
        _targetOffset = Random.insideUnitSphere * _itanoWobbleIntensity;
    }

    public override void FuckingExplode()
    {
        DetachSmokeTrail();
        base.FuckingExplode();
    }

    private void DetachSmokeTrail()
    {
        if ( _smokeTrail )
        {
            Debug.Log( "smoketrail is trail" );
            _smokeTrail.AddComponent<FXKiller>().duration = 5f;
            if ( _smokeTrail.TryGetComponent<FXKiller>( out FXKiller killer ) )
            {
                Debug.Log( "smoketrail is trail" );
            }
            _smokeTrail.transform.SetParent( null );
            _smokeTrail = null;
        }
    }

    private void OnDrawGizmos()
    {
        if ( _gizmos && _rb && _target )
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawSphere( _target.transform.position + _targetOffset, 0.3f );
            Gizmos.DrawLine( transform.position, transform.position + _rb.velocity );

        }
    }

    protected override void OnCollisionEnter( Collision collision )
    {
        FuckingExplode();
    }
}
