﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosive : Projectile
{
    [SerializeField] protected Transform explosionFX;
    public float explosionRadius;


    // Update is called once per frame
    protected override void FixedUpdate()
    {
        LifeSpanTimer();
    }

    protected void SetLeftoverLifeSpan( float newMaxLifeSpan )
    {
        if ( _maxLifeSpan - _timeAlive >= newMaxLifeSpan )
        {
            _maxLifeSpan = _timeAlive + newMaxLifeSpan;
        }
    }

    public virtual void FuckingExplode()
    {
        Instantiate( explosionFX, gameObject.transform.position, Quaternion.identity );
        Collider[] cols = Physics.OverlapSphere( transform.position, explosionRadius, GlobalValues.LayerMasks.AgentMask );

        foreach ( Collider col in cols )
        {
            if ( col.TryGetComponent( out Enemy enemy ) )
            {
                enemy.TakeDamage( _damage, _origin );
            }
        }
        Destroy( gameObject );
    }
}
