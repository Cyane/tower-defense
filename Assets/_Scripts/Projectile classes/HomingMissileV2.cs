﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingMissileV2 : Explosive
{
    public Transform target;
    public Transform launcher;

    [SerializeField] private List<GameObject> _jetEngines = new List<GameObject>();

    [SerializeField] private GameObject _smokeTrail;

    [Header( "Forward force values" )]
    [SerializeField] private float _maxSpeed;
    [SerializeField] private float _force;

    [Header( "Rotational force values" )]
    [SerializeField] private float _steeringThreshold;//degrees it's pointing away from it's target before adjusting 
    [SerializeField] private float _rotationForce;
    [SerializeField] private float _maxAngularVelocity;
    [SerializeField] private float _angularDrag;

    [Header( "Perlin randomness" )]
    [SerializeField] private bool _addRandomWobble;//enable or disable altogether
    [SerializeField] private float _perlinMultiplier;//strength of the noise
    [SerializeField] private float _perlinScale;//how "smooth" the noise will be
    private Vector3 _seed;//each missile has its own seed for perlin wobble. 
    // three seeds is overkill

    private bool tracking;
    private bool collisionEnabled = false;
    private Vector3 _targetPos;

    protected override void Start()
    {
        base.Start();
        _seed = new Vector3( Random.Range( -1000f, 1000f ), Random.Range( -1000f, 1000f ), Random.Range( -1000f, 1000f ) );
        _rb.maxAngularVelocity = _maxAngularVelocity;
        _rb.angularDrag = _angularDrag;
        if ( TryGetComponent<Collider>( out Collider col ) ) { col.enabled = false; }

        tracking = true;
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();


        if ( tracking )
        {
            if ( target != null )
            {
                HomeInOnTargetV2();
            }
            else
            {
                DeactivateMissile();
                //JustGoStraight();
            }
        }

        if ( !collisionEnabled )
        {
            LauncherDistanceCheck();
        }
    }

    protected override void OnCollisionEnter( Collision collision )
    {
        base.OnCollisionEnter( collision );
        FuckingExplode();
    }

    protected void HomeInOnTargetV2()
    {
        _targetPos =
            target.position
            + GetPerlinValues()
            * Vector3.Distance( target.position, transform.position )
            * 0.05f;

        Quaternion targetRot = Quaternion.FromToRotation( transform.up, (_targetPos - transform.position).normalized );

        //add velocity. rockets go woosh
        _rb.velocity = Vector3.ClampMagnitude( _rb.velocity + transform.up * _force, _maxSpeed );

        float angleInDegrees;
        Vector3 rotationAxis;
        targetRot.ToAngleAxis( out angleInDegrees, out rotationAxis );
        Vector3 angularDisplacement = rotationAxis * angleInDegrees * Mathf.Deg2Rad;

        SetJetIntensity( angularDisplacement );

        _rb.angularVelocity =
            _rb.angularVelocity
            + (angularDisplacement
            * angularDisplacement.magnitude
            * _rotationForce
            * 0.1f);
    }

    private void SetJetIntensity( Vector3 angularDisplacement )
    {
        Vector3 crossProduct = Vector3.Cross( angularDisplacement, transform.up );//seems to work as intended

        for ( int i = 0; i < _jetEngines.Count; i++ )
        {
            Vector3 jetDir = Quaternion.AngleAxis( 45 + 90 * i, transform.up ) * transform.forward;

            float magnitudeSquared = Mathf.Pow( Vector3.Magnitude( jetDir + crossProduct ), 2f );
            //float magnitude = Vector3.Magnitude(jetDir + crossProduct);
            _jetEngines[ i ].transform.localScale.Set( magnitudeSquared, magnitudeSquared, magnitudeSquared );
        }

        //Debug.Log("Angular displacement vector: " + angularDisplacement + " Up: " + transform.up + " Cross product " + crossProduct);
    }

    private void JustGoStraight()
    {
        _rb.velocity = Vector3.ClampMagnitude( _rb.velocity + transform.up * _force, _maxSpeed );
    }

    private void DeactivateMissile()
    {
        tracking = false;
        _smokeTrail.GetComponent<ParticleSystem>().Stop();
        //shorten lifespan and drop to the floor
        SetLeftoverLifeSpan( Random.Range( 1f, 2f ) );
        _rb.useGravity = true;
    }

    /// <summary>
    /// Checks if there's a bit of distance between the launcher and the missile before turning the collider on to prevent it from colliding with the launcher itself and exploding right away.
    /// </summary>
    private void LauncherDistanceCheck()
    {
        if ( !launcher || Vector3.Distance( transform.position, launcher.position ) > 1.5f )
        {
            GetComponent<Collider>().enabled = true;
            collisionEnabled = true;
        }
    }



    private void DetachSmokeTrail()
    {
        if ( _smokeTrail )
        {
            _smokeTrail.AddComponent<FXKiller>().duration = 5f;
            _smokeTrail.transform.parent = null;
            _smokeTrail = null;
        }
    }

    Vector3 GetPerlinValues()
    {
        Vector3 perlin = Vector3.zero;

        if ( _addRandomWobble )
        {
            Vector3 pos = (transform.position + _seed) * _perlinScale;
            perlin = new Vector3(
                Mathf.PerlinNoise( pos.y, pos.z ) * 2 - 1,
                Mathf.PerlinNoise( pos.x, pos.z ) * 2 - 1,
                Mathf.PerlinNoise( pos.x, pos.y ) * 2 - 1
                );
            perlin *= _perlinMultiplier;
        }
        return perlin;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere( _targetPos, 1f );
    }
}
