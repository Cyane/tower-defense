using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Projectile : MonoBehaviour
{
    protected float _damage;
    protected Vector3 _velocity;
    [SerializeField] protected float _maxLifeSpan = 10f;
    protected float _timeAlive = 0f;
    protected Rigidbody _rb;
    protected Tower _origin;
    protected Enemy _target;

    protected virtual void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    protected virtual void FixedUpdate()
    {
        _rb.MovePosition( transform.position + _velocity * Time.fixedDeltaTime );
        LifeSpanTimer();
    }

    protected void LifeSpanTimer()
    {
        _timeAlive += Time.deltaTime;
        if ( _timeAlive >= _maxLifeSpan )
        {
            OnEndOfLifetime();
        }
    }

    protected virtual void OnEndOfLifetime()
    {

    }

    public virtual void Init( Tower origin, float damage, Vector3 velocity, Enemy target)
    {
        _origin = origin;
        _damage = damage;
        _velocity = velocity;
        _target = target;
    }

    protected virtual void OnCollisionEnter( Collision collision )
    {
        Debug.Log( $"I'm colliding with {collision.gameObject.name}" );
        if ( collision.transform.TryGetComponent( out Enemy enemy ) )
        {
            enemy.TakeDamage( _damage, _origin );
        }
        Destroy( gameObject );
    }

    protected virtual void OnDestroy()
    {
        //spawn an effect or something
    }
}
