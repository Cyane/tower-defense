using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class Tile : MonoBehaviour
{
    [field: SerializeField] public string TileId { get; private set; }
    [field: SerializeField] public bool IsBuildable { get; private set; }
    [field: SerializeField] public bool IsWalkable { get; private set; }
    [field: SerializeField] public bool IsFlyable { get; private set; }
    [field: SerializeField] public bool IsSwimmable { get; private set; }
    [field: SerializeField] public bool IsSpawnerTile { get; private set; }
    [field: SerializeField] public bool IsGoalTile { get; private set; }
    [field: SerializeField] public bool IsTransparent { get; private set; }
    
    [SerializeField] private TileType _tileType;
    public TileType TileType { get => _tileType; }
    
    public Tile (TileType tileType)
    {
        _tileType = tileType;
    }
}
public enum TileType
{
    RoughFlat,
    BuildableFlat,
    Water,
    Mountain,
    Spawner,
    Goal
}
