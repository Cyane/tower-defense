using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StructureButton : ItemButton 
{
    private Structure _structure;
    [SerializeField] private TextMeshProUGUI _priceText;
    
    
    public override void Init(GameObject structureItem) 
    {
        if (structureItem.TryGetComponent<Structure>(out Structure structure))
        {
            base.Init(structureItem);
            _structure = structure;
            SetName(structure.name);
            _priceText.text = structure.price.ToString("0");            
        }
        else
        {
            Destroy (gameObject);
        }
    }

    private void OnEnable()
    { 
        if (_structure != null)
        {
            CheckAffordability();
            GameManager.instance.GoldAmountChanged += CheckAffordability;
        }
    }

    private void OnDisable()
    {
        Debug.Log("on disable called");
        GameManager.instance.GoldAmountChanged -= CheckAffordability;
    }

    private void OnDestroy()
    {
        Debug.Log("ERROR: No Structure component found. Committing sudoku.");
    }

    private void CheckAffordability()
    {
        if (_structure.price > GameManager.instance.Gold)
        {
            if (_button.IsInteractable())
            {
                _button.interactable = false;
            }
        }
        else if (!_button.IsInteractable())
        {
            _button.interactable = true;
        }
    }
}
