﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class StateMachine 
{
	private GameObject owner;
	public StateMachine(GameObject owner)
	{
		this.owner = owner;
	}

	private State currentState;
	public string getCurrentState()
	{
		return currentState.GetType().Name;
	}

	public void TransitionState(State state)
	{
		if (currentState != null)
			currentState.OnExitState();
		currentState = state;
		currentState.owner = this.owner;
		currentState.OnEnterState();
	}

	public void OnUpdate()
	{
		if (currentState != null)
			currentState.OnUpdate();
	}
	
	public void OnFixedUpdate()
	{
		if (currentState != null)
			currentState.OnFixedUpdate();
	}
}

public abstract class State
{
	public GameObject owner;
	public virtual void OnEnterState() { }
	public virtual void OnExitState() { }
	public virtual void OnUpdate() { }
	public virtual void OnFixedUpdate() { }
}








