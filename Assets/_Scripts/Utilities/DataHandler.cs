using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

// Credits to Michelle Fuchs/Velvary on youtube for Json handling functions
public static class DataHandler
{

    //private static int saltInterval = 7;
    private static int upperCase = 97;
    private static int lowerCase = 65;
    private static int caseFlipFlop( int i ) => i % 2 == 0 ? upperCase : lowerCase;
    public static string SaltString( string blandString, int saltInterval )
    {
        if ( saltInterval > 999 || saltInterval < 1 ) { return ""; }

        StringBuilder builder = new StringBuilder();
        builder.Append( Convert.ToBase64String( Encoding.UTF8.GetBytes( blandString ) ) );
        for ( int i = 1; i <= builder.Length / saltInterval; i++ )
        {
            if ( i % 3 == 0 )
            {
                // insert number every third time
                builder.Insert(
                    i * saltInterval - 1,
                    Mathf.RoundToInt( UnityEngine.Random.Range( 0, 9 ) ).ToString()
                );
            }
            else
            {
                builder.Insert(
                    i * saltInterval - 1,
                    Mathf.RoundToInt( UnityEngine.Random.Range( 0, 26 ) + caseFlipFlop( i ) )
                );
            }
        }
        // First three characters indicate interval of salt characters
        builder.Insert( 0, saltInterval.ToString().PadLeft( 3, '0' ) );
        return builder.ToString();
    }

    public static string RemoveSalt( string saltyString )
    {
        StringBuilder builder = new StringBuilder();
        int saltInterval = int.Parse( saltyString.Substring( 0, 3 ) );
        builder.Append( saltyString );
        builder.Remove( 0, 3 );

        for ( int i = 0; i < saltyString.Length / saltInterval; i++ )
        {
            // builder.Remove()
        }
        builder.Append( saltyString );
        //builder.Append( Convert.ToChar( (int) UnityEngine.Random.Range( 0, 26 ) + TextCase( i ) ) );
        // Decode a base64 string back into a string
        byte[] decodedBytes = Convert.FromBase64String( builder.ToString() );
        return Encoding.UTF8.GetString( decodedBytes );
    }

    public static void SaveToJSON<T>( List<T> toSave, string filename )
    {
        Debug.Log( GetPath( filename ) );
        string content = JsonHelper.ToJson<T>( toSave.ToArray() );
        WriteFile( GetPath( filename ), content );
    }

    public static void SaveToJSON<T>( T toSave, string filename )
    {
        string content = JsonUtility.ToJson( toSave );
        WriteFile( GetPath( filename ), content );
    }

    public static List<T> ReadListFromJSON<T>( string filename )
    {
        string content = ReadFile( GetPath( filename ) );

        if ( string.IsNullOrEmpty( content ) || content == "{}" )
        {
            return new List<T>();
        }

        List<T> res = JsonHelper.FromJson<T>( content ).ToList();

        return res;

    }

    public static T ReadFromJSON<T>( string filename )
    {
        string content = ReadFile( GetPath( filename ) );

        if ( string.IsNullOrEmpty( content ) || content == "{}" )
        {
            return default( T );
        }

        T res = JsonUtility.FromJson<T>( content );

        return res;

    }

    private static string GetPath( string filename )
    {
        return Application.persistentDataPath + "/" + filename;
    }

    private static void WriteFile( string path, string content )
    {
        FileStream fileStream = new FileStream( path, FileMode.Create );

        using ( StreamWriter writer = new StreamWriter( fileStream ) )
        {
            writer.Write( content );
        }
    }

    private static string ReadFile( string path )
    {
        if ( File.Exists( path ) )
        {
            using ( StreamReader reader = new StreamReader( path ) )
            {
                string content = reader.ReadToEnd();
                return content;
            }
        }
        return "";
    }
}

public static class JsonHelper
{
    public static T[] FromJson<T>( string json )
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>( json );
        return wrapper.Items;
    }

    public static string ToJson<T>( T[] array )
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson( wrapper );
    }

    public static string ToJson<T>( T[] array, bool prettyPrint )
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson( wrapper, prettyPrint );
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}

