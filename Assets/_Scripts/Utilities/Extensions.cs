using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static Vector2 ToV2DiscardX( this Vector3 input ) => new Vector2( input.y, input.z );
    public static Vector2 ToV2DiscardY( this Vector3 input ) => new Vector2( input.x, input.z );
    public static Vector2 ToV2DiscardZ( this Vector3 input ) => new Vector2( input.x, input.y );
}
