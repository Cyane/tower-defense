using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GlobalValues
{
    // Use these to get color codes and to convert them to floats between 0 and 1
    // https://htmlcolorcodes.com/
    // https://www.rapidtables.com/convert/color/hex-to-rgb.html
    public static class Colors
    {
        public static Color DarkRed = new Color( 0.745f, 0f, 0f );
        public static Color Orange = new Color( 1f, 0.5f, 0f );
        public static Color Lime = new Color( 0.58f, 0.87f, 0f );
        public static Color Green = new Color( 0f, 0.93f, 0f );
    }

    public static class LayerMasks
    {
        public static LayerMask UIMask => LayerMask.GetMask( "UI" );
        public static LayerMask AgentMask => LayerMask.GetMask( "Agents" );
        public static LayerMask TileMask => LayerMask.GetMask( "Tiles" );
        public static LayerMask StructureMask => LayerMask.GetMask( "Structures" );
        public static LayerMask BuildingGridMask => LayerMask.GetMask( "BuildingGrid" );
    }
}

