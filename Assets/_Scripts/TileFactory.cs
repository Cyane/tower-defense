using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TileFactory
{
    private static Dictionary<string, GameObject> tileDictionary;
    static TileFactory()
    {
        GameObject[] tiles = Resources.LoadAll<GameObject>( "Prefabs/Tiles" );
        tileDictionary = new Dictionary<string, GameObject>( tiles.Length );

        foreach ( GameObject tile in tiles )
        {
            tileDictionary.Add( tile.GetComponent<Tile>().TileId, tile );
        }
    }

    public static GameObject InstantiateTileFromId( string tileId )
    {
        if ( tileDictionary.TryGetValue( tileId, out GameObject tilePrefab ) )
        {
            return Object.Instantiate( tilePrefab );
        }
        else
        {
            Debug.LogError( "Tile prefab with ID " + tileId + "could not be found." );
            return null;
        }
    }
}
