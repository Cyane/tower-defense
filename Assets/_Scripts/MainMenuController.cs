using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadLevelEditor()
    {
        SceneManager.LoadScene("scn_LevelBuilder");
    }
}

/*  Add profile selection menu
 *  Add stat screen per profile
 *  
 *  Options menu settings:
 *  General
 *      Gore
 *  Graphics
 *      Vsync
 *      Main display
 *      Resolution
 *      Bloom
 *  Audio
 *      SFX Volume
 *      Music volume
*/
