using System;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Enemy : MonoBehaviour
{
    [field: SerializeField] public float CurrentHealth { get; private set; }
    [field: SerializeField] public float MaxHealth { get; private set; }
    public float speed;
    public float goldValue;
    public Transform target;
    private EnemyUI _enemyUI;
    private NavMeshAgent _agent;
    private float _minTargetDist = 0.3f;// distance to target before damage is dealt
    
    public float TimeUntilDestination  => _agent.remainingDistance / speed;
    
    public event Action<float> HealthChanged;
    public event Action<float, DamageType> DamageTaken; // for flying damage numbers (TODO)
    public event Action<Enemy> Death;
    
    public Enemy(Transform target) // Make constructor take all the parameters so that we can make a cool enemy factory
    {
        this.target = target;
    }

    void Start()
    {
        _enemyUI = GetComponentInChildren<EnemyUI>();
        _agent = GetComponent<NavMeshAgent>();
        _agent.destination = target.position;
    }

    void FixedUpdate()
    {
        if(Vector3.Distance(transform.position, target.position) < _minTargetDist)
        {
            DamageTarget();
        }
    }

    public void TakeDamage(float damage, Tower origin)// TODO: add damage types to projectile and turret etc
    {
        DamageTaken?.Invoke(damage, DamageType.Normal);
        if (damage >= CurrentHealth)
        {
            origin.RegisterDamageDealt(CurrentHealth);
            CurrentHealth = 0f;
            Unalive(true);
        }
        else
        {
            origin.RegisterDamageDealt(damage);
            CurrentHealth -= damage;
            HealthChanged?.Invoke(CurrentHealth);
        }
        _enemyUI.SpawnDamageNumber(damage, DamageType.Normal);
        _enemyUI.UpdateHealth(MaxHealth, CurrentHealth);
    }
    protected void DamageTarget()
    {
        // TODO: make damagable targets to protect and keep track of
        Debug.Log("Oh nyo! The enemy reached our thing!");
        Unalive(false);
    }

    public void Unalive(bool killed)
    {
        if (killed)
        {
            GameManager.instance.AdjustGold(goldValue);
        }
        Death?.Invoke(this);
        Destroy( gameObject );
        //agent.enabled = false;
        //GetComponent<Collider>().enabled = false;
        //await PlayAnimation();

    }

    protected virtual void UnsubscribeAll()
    {
        Death = null;
        HealthChanged = null;
        DamageTaken = null;
    }

    protected virtual void OnDisable()
    {
        UnsubscribeAll();
    }

    /*
    protected async Task PlayAnimation()
    {
        await Task.Delay(500);
    }
    */


}

public enum EnemyType
{
    Ground,
    Flying,
    Amphibian
}

public enum DamageType // todo: move to a file that makes more sense
{
    Normal,
    Critical,
    Shield,
    Poison,
    Fire
}
