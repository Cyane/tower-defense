using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Text;
using System;

public class LevelBuildingController : MonoBehaviour
{
    public Vector2Int gridStartingSize;

    public LayerMask buildGridMask, tileMask;
    [SerializeField] private GameObject _selectedTile;
    [SerializeField] private Material _gridLineMaterial;
    private GameObject _level;
    private BuildGrid _tileGrid;
    private int TextCase( int i ) => i % 2 == 0 ? 65 : 97;
    private GameObject _previewObject;
    [SerializeField] private List<Button> growButtons = new List<Button>();

    public static LevelBuildingController instance { get; private set; }

    void Awake()
    {
        if ( instance != null && instance != this )
        {
            Destroy( this );
        }
        else
        {
            instance = this;
        }
    }

    private void Start()
    {
        if ( _tileGrid == null )
        {
            GameObject tileGridObject = new GameObject( "Tiles" );
            tileGridObject.AddComponent<BuildGrid>().Init( gridStartingSize );
            _tileGrid = tileGridObject.GetComponent<BuildGrid>();
        }
        //InitializeBuildGrid();
        InitButtons();
    }

    private void Update()
    {
        PreviewTilesOnGrid();
        InputHandling();
    }

    private void InitButtons()
    {
        if ( growButtons.Count >= 4 )
        {
            growButtons[ 0 ].onClick.AddListener( () => _tileGrid.ResizeTileGrid( 0, 0, 0, 1 ) );
            growButtons[ 1 ].onClick.AddListener( () => _tileGrid.ResizeTileGrid( 0, 1, 0, 0 ) );
            growButtons[ 2 ].onClick.AddListener( () => _tileGrid.ResizeTileGrid( 1, 0, 0, 0 ) );
            growButtons[ 3 ].onClick.AddListener( () => _tileGrid.ResizeTileGrid( 0, 0, 1, 0 ) );
        }
    }

    private void InputHandling()
    {
        if ( Input.GetButton( "Fire1" ) )
        {
            PlaceSelectedTile();
        }
        if ( Input.GetButton( "Fire2" ) )
        {
            DeleteTile();
        }
    }

    public void CropGrid()
    {
        if ( _tileGrid != null )
        {
            _tileGrid.TrimEmptyGridSpace();
            //SetBuildingPlaneTransform();
            //_buildingPlane.SetActive(false);
        }
    }



    public void SetSelectedTile( GameObject selectedTile )
    {
        _selectedTile = selectedTile;
        if ( _previewObject != null ) { Destroy( _previewObject ); }
        _previewObject = Instantiate( _selectedTile );

        foreach ( Collider col in _previewObject.GetComponentsInChildren<Collider>() )
        {
            col.enabled = false;
        }
        _previewObject.SetActive( false );
        Debug.Log( _selectedTile.name );
    }

    private void PreviewTilesOnGrid()
    {
        if ( _previewObject != null )
        {
            if ( !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()
                && Physics.Raycast( Camera.main.ScreenPointToRay( Input.mousePosition ), out RaycastHit hit, Mathf.Infinity, buildGridMask ) )
            {
                Vector2Int coords = _tileGrid.WorldPosToGridCoords( hit.point );
                if ( _tileGrid.TileArray[ coords.x, coords.y ] == null )
                {
                    if ( !_previewObject.activeSelf )
                    {
                        _previewObject.SetActive( true );
                    }
                    _previewObject.transform.position = new Vector3( Mathf.Floor( hit.point.x ) + 0.5f, _tileGrid.transform.position.y, Mathf.Floor( hit.point.z ) + 0.5f );
                }
                else if ( _previewObject.activeSelf )
                {
                    _previewObject.SetActive( false );
                }
            }
            else if ( _previewObject.activeSelf )
            {
                _previewObject.SetActive( false );
            }
        }
    }

    void PlaceSelectedTile()
    {
        if ( _selectedTile != null
            && _previewObject.activeSelf // using previewObject as a check
            && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() )
        {
            if ( Physics.Raycast( Camera.main.ScreenPointToRay( Input.mousePosition ), out RaycastHit hit, Mathf.Infinity, buildGridMask ) )
            {
                Vector2Int coords = _tileGrid.WorldPosToGridCoords( hit.point );
                _tileGrid.AddTileToGrid( coords, Instantiate( _selectedTile, _previewObject.transform.position, Quaternion.identity, _tileGrid.transform ).GetComponent<Tile>() );
            }
        }
    }

    void DeleteTile()
    {
        if ( !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() )
        {
            if ( Physics.Raycast( Camera.main.ScreenPointToRay( Input.mousePosition ), out RaycastHit hit, Mathf.Infinity, buildGridMask ) )
            {
                Vector2Int coords = _tileGrid.WorldPosToGridCoords( hit.point );
                if ( _tileGrid.TileArray[ coords.x, coords.y ] != null )
                {
                    Destroy( _tileGrid.TileArray[ coords.x, coords.y ].gameObject );
                }
            }

        }
    }

    public void LoadMainScene()
    {
        SceneManager.LoadScene( "scn_Game" );
    }


}
