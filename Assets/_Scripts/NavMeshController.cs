using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.AI.Navigation;

public class NavMeshController : MonoBehaviour
{
    public NavMeshSurface groundSurface;
    public NavMeshSurface aerialSurface;
    void Awake()
    {
        NavMeshSurface[] surfaces = GetComponents<NavMeshSurface>();
        if( surfaces.Length >= 2)
        {
            groundSurface = surfaces[0];
            aerialSurface = surfaces[1];
        }
        else
        {
            Debug.Log("NavmeshController has no NavMesh Surfaces attached.");
        }

    }

    public void BuildNavMesh()
    {
        if (groundSurface != null)
        {
            groundSurface.BuildNavMesh();
            Debug.Log("Ground unit NavMesh built!");
        }
        if (aerialSurface != null)
        {
            aerialSurface.BuildNavMesh();
            Debug.Log("Air unit NavMesh built!");
        }
    }
}
