using System;
using UnityEngine;

[Serializable, CreateAssetMenu( fileName = "Tower", menuName = "Scriptable Objects/Tower" )]
public class TowerSO : ScriptableObject
{
    [field: SerializeField] public Tower prefab                     {get; private set;}
    [field: SerializeField] public string towerName                 {get; private set;}
    [field: SerializeField] public string description               {get; private set;}
    [field: SerializeField] public bool targetAirUnits              {get; private set;}
    [field: SerializeField] public bool targetGroundUnits           {get; private set;}
    [field: SerializeField] public int basePrice                    {get; private set;}
    [field: SerializeField] public float upgradePriceMultiplier     {get; private set;}
    [field: SerializeField] public float upgradePriceFlatIncrease   {get; private set;}
    [field: SerializeField] public TowerStat damage                 {get; private set;}
    [field: SerializeField] public float minRange                   {get; private set;}
    [field: SerializeField] public TowerStat maxRange               {get; private set;}
    [field: SerializeField] public TowerStat rateOfFire             {get; private set;}
    [field: SerializeField] public TowerStat reloadSpeed            {get; private set;}
    [field: SerializeField] public TowerStat areaOfEffect           {get; private set;}
}


// refactor:
// change to ints maybe?
[Serializable]
public struct TowerStat
{
    public float value;
    public bool shownInUi;
    public bool upgradable;
    public float upgradeMaxValue;
    public float upgradeMinValue;
    public float upgradeLevelMultiplier; // upgrade value * tower level * level multiplier
    //float upgradeChanceWeight;
}