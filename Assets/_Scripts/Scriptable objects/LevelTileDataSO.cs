using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelTileData", menuName = "Scriptable Objects/Level tile data")]
public class LevelTileDataSO : ScriptableObject
{
    public string[,] tileIdArray;
}

//https://docs.unity3d.com/ScriptReference/ScriptableObject.html
