using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "TileSetSO", menuName = "Scriptable Objects/Tile prefab set")]
public class TileIdSO : ScriptableObject
{
    [SerializeField] private GameObject _roughTile;
    [SerializeField] private GameObject _buildableTile;
    [SerializeField] private GameObject _waterTile;
    [SerializeField] private GameObject _mountainTile;
    [SerializeField] private GameObject _spawnerTile;
    [SerializeField] private GameObject _goalTile;

    public Tile GetTilePrefab(TileType tileType)
    {
        switch (tileType)
        {
            case TileType.RoughFlat:
                return _roughTile.GetComponent<Tile>();
            case TileType.BuildableFlat:
                return _buildableTile.GetComponent<Tile>();
            case TileType.Water:
                return _waterTile.GetComponent<Tile>();
            case TileType.Mountain:
                return _mountainTile.GetComponent<Tile>();
            case TileType.Spawner:
                return _spawnerTile.GetComponent<Tile>();
            case TileType.Goal:
                return _goalTile.GetComponent<Tile>();
            default:
                Debug.LogError("Tileset does not contain a Tile for requested tile type");
                return null;
        }
    }
}
