using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TileButton : MonoBehaviour
{
    public Tile tile;
    private Button _button;

    private void Start()
    {
        if (tile != null)
        {
            GetComponentInChildren<TextMeshProUGUI>().text = tile.name;
            _button = gameObject.GetComponent<Button>();
            _button.onClick.AddListener(() => _button.Select());
            _button.onClick.AddListener(() => LevelBuildingController.instance.SetSelectedTile(tile.gameObject));

        }
        else
        {
            Debug.Log("ERROR: No Tile assigned to button. Committing sudoku.");
            Destroy(gameObject);
        }
    }
}
