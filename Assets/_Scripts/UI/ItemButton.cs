using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Button))]
public abstract class ItemButton : MonoBehaviour
{
    protected Button _button;
    [SerializeField] protected TextMeshProUGUI _itemName;

    
    public virtual void Init(GameObject item)
    {
        _button = gameObject.GetComponent<Button>();
        _itemName = gameObject.GetComponentInChildren<TextMeshProUGUI>();
        _button.onClick.AddListener(() => _button.Select());
        _button.onClick.AddListener(() => BuildingManager.Instance.SetSelectedObject(item));
    }

    public void SetName(string itemName)
    {
        _itemName.text = itemName;
    }
}
