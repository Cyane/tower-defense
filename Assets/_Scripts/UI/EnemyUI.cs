using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EnemyUI : MonoBehaviour
{
    private Canvas _canvas;
    private Slider _healthbar;
    [SerializeField] private DamageNumber _damageNumberPrefab;
    void Start()
    {
        _canvas = GetComponent<Canvas>();
        _healthbar = GetComponentInChildren<Slider>();
        _healthbar.gameObject.SetActive(false);
    }

    public void UpdateHealth(float maxHP, float currentHP)
    {
        if(!_healthbar.gameObject.activeSelf) { _healthbar.gameObject.SetActive(true); }
        _healthbar.maxValue = maxHP;
        _healthbar.value = currentHP;
    }

    public void SpawnDamageNumber(float damage, DamageType damageType)
    {
        DamageNumber damageNumber = Instantiate(_damageNumberPrefab, _canvas.transform);
        damageNumber.Init(damage, damageType);
    }

    void Update()
    {
        Debug.Log(CameraManager.Instance.name);

        Vector3 newRotation = Quaternion.LookRotation(CameraManager.Instance.MainCam.transform.forward).eulerAngles;
        //newRotation.x = -CameraManager.instance.mainCam.transform.rotation.x;
        transform.eulerAngles = newRotation;
        if (CameraManager.Instance.IsOrthogonal)
        {
            //transform.rotation.SetLookRotation(new Vector3(45, 45, 0));
        }
        else
        {
            //_canvas.transform.LookAt(CameraManager.instance.mainCam.transform.position);
        }
        
    }
}
