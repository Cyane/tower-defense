using GD.MinMaxSlider;
using GlobalValues;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class DamageNumber : MonoBehaviour
{
    private TextMeshProUGUI _text;
    private CanvasGroup _canvasGroup;
    private RectTransform _rectTransform;
    [Header("General variables")]
    [SerializeField] private float _totalLifeSpan;
    [SerializeField] [MinMaxSlider(-15, 15)] private Vector2 _randomSpeedRangeX;
    [SerializeField] [MinMaxSlider(0, 20)] private Vector2 _randomSpeedRangeY;
    [Header("Second phase")]
    [SerializeField] private float _secondPhaseStart;
    [SerializeField] private float _secondPhaseFloatingSpeed;
    [Header("Changes over lifetime")]
    [SerializeField] private AnimationCurve _alphaOverLifetime;
    [SerializeField] private AnimationCurve _sizeOverLifetime;
    [SerializeField] private AnimationCurve _speedOverLifetimeY;
    [SerializeField] private AnimationCurve _speedOverLifetimeX;

    private float _timeLeftAlive;
    private Vector3 _velocity;
    private Vector3 _worldPosOnStart;
    private Vector3 _positionOffset;

    private void Awake() // Awake is executed before instantiation returns
    {
        _text = GetComponent<TextMeshProUGUI>();
    }

    void Start()
    {
        //_determinedSpeedY = Mathf.Lerp(_randomSpeedY.x, _randomSpeedY.y, Random.Range(0, 1));
        //_determinedSpeedX = Mathf.Lerp(_randomSpeedX.x, _randomSpeedX.y, Random.Range(0, 1));
        _rectTransform = GetComponent<RectTransform>();
        _rectTransform.anchoredPosition = Vector3.zero;
        _canvasGroup = GetComponent<CanvasGroup>();
        _worldPosOnStart = _rectTransform.position;
        _timeLeftAlive = _totalLifeSpan;
        _velocity = new Vector3( Random.Range(_randomSpeedRangeX.x, _randomSpeedRangeX.y), Random.Range(_randomSpeedRangeY.x, _randomSpeedRangeY.y), 0f );
    }

    public void Init(float number, DamageType type)
    {
        _text.text = Mathf.Round(number).ToString();

        //todo: refactor to take these values, or a material, from a scriptable object instead
        /*
        switch (type)
        {
            case DamageType.Normal:
                _text.color = Colors.DarkRed;
                break;
            case DamageType.Critical:
                _text.color = Colors.Orange;
                _text.fontStyle = FontStyles.Bold;
                break;
            case DamageType.Poison:
                _text.color = Colors.Lime;
                break;
            default:
                _text.color = Colors.DarkRed;
                break;
        }
        */
    }

    void Update()
    {
        _timeLeftAlive -= Time.deltaTime;
        if (_timeLeftAlive <= 0)
        {
            Destroy(gameObject);
            return;
        }
        float lifespanValue =  1f - Mathf.Clamp01(_timeLeftAlive / _totalLifeSpan);
        _canvasGroup.alpha = _alphaOverLifetime.Evaluate(lifespanValue);
        _rectTransform.localScale =  Vector3.one * _sizeOverLifetime.Evaluate(lifespanValue);

        // if second phase started, just float up
        if(_totalLifeSpan - _timeLeftAlive >= _secondPhaseStart)
        {
            _positionOffset += new Vector3( 0f, _secondPhaseFloatingSpeed, 0f) * Time.deltaTime * (1f / transform.parent.localScale.x);
        }
        else // launch in direction until second phase
        {
            // disabled speed over lifetime
            //Vector3 speedMultiplier = new Vector3(_speedOverLifetimeX.Evaluate(lifespanValue), _speedOverLifetimeY.Evaluate(lifespanValue), 0f);
            _positionOffset += (_velocity) * Time.deltaTime * (1f / transform.parent.localScale.x);
        }
        _rectTransform.localPosition = _positionOffset + transform.parent.InverseTransformPoint(_worldPosOnStart);
    }
}


