using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = false)]
public class EnumFlagsFieldAttribute : PropertyAttribute
{

    private string[] _maskNames;

    public EnumFlagsFieldAttribute(params string[] names)
    {
        _maskNames = names;
    }

    public string[] MaskNames { get { return _maskNames; } }

}
/*
 * https://forum.unity.com/threads/how-to-make-a-serializableproperty-with-a-maskfield.280702/
 * https://github.com/lordofduct/spacepuppy-unity-framework/blob/master/SpacepuppyBase/PropertyAttributes.cs
 * https://docs.unity3d.com/ScriptReference/EditorGUILayout.EnumFlagsField.html
*/
