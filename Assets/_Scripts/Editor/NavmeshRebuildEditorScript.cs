using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(NavMeshController))]
public class NavmeshRebuildEditor : Editor
{
    public override void OnInspectorGUI()
    {
        // Draw the default inspector UI
        DrawDefaultInspector();

        // Get a reference to the NavMeshController script
        NavMeshController navMeshController = (NavMeshController)target;

        // Add a button to the inspector
        if (GUILayout.Button("Build NavMesh"))
        {
            // Call the method to rebuild the NavMesh when the button is pressed
            navMeshController.BuildNavMesh();
        }
    }
}
