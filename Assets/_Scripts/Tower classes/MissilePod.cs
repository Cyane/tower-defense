using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissilePod : MonoBehaviour
{
    //ParticleSystem 
    [field: SerializeField] public Transform tubeEnd { get; private set; }
    private Animator _animator;
    private float _reloadTime;
    public float RemainingCooldown { get; private set; }
    public bool ReadyToFire { get; private set; }
    // public float ReloadAnimTime { get => 0.3f; } // 15 frames of animation at 50 samples per second

    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    public void SetReloadTime( float reloadTime )
    {
        _reloadTime = reloadTime;
    }

    public void MissilePodFixedUpdate() // call from parent
    {
        if(RemainingCooldown > 0f)
        {
            RemainingCooldown -= Time.fixedDeltaTime;
            _animator.SetFloat("Cooldown", RemainingCooldown);
        }
        else if (!ReadyToFire)
        {
            ReadyToFire = true;
        }
    }

    /// <summary> </summary>
    public void LaunchMissile()
    {
        if (ReadyToFire)
        {
            //play particle effect
            _animator.SetFloat("Cooldown", _reloadTime);
            _animator.SetTrigger("LaunchMissile");
            RemainingCooldown = _reloadTime;
            ReadyToFire = false;
        }
    }
}
