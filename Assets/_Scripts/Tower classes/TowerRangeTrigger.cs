using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class TowerRangeTrigger : MonoBehaviour
{
    [SerializeField] private Collider _triggerCol;
    [SerializeField] private Rigidbody _rb;

    private List<Enemy> _enemiesInRange = new List<Enemy>();
    public List<Enemy> EnemiesInRange => _enemiesInRange;

    private void Awake()
    {
        _triggerCol = GetComponent<Collider>();
        _rb = GetComponent<Rigidbody>();
        _rb.isKinematic = true;
    }

    private void OnTriggerEnter( Collider other )
    {
        if ( other.gameObject.TryGetComponent<Enemy>( out Enemy enemy ) )
        {
            enemy.Death += RemoveFromList;
            _enemiesInRange.Add( enemy );
        }
    }

    private void OnTriggerExit( Collider other )
    {
        if ( other.gameObject.TryGetComponent<Enemy>( out Enemy enemy ) )
        {
            RemoveFromList( enemy );
        }
    }

    private void RemoveFromList( Enemy enemy )
    {
        enemy.Death -= RemoveFromList;
        _enemiesInRange.Remove( enemy );
    }


}
