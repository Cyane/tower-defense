using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MissileLauncher : Tower
{
    [field: SerializeField] public float ReloadSpeed { get; private set; }
    [SerializeField] private HomingMissile _missilePrefab;
    [SerializeField] private List<MissilePod> _missilePods = new List<MissilePod>();
    private int _nextPodIndexToFire = 0;

    protected override void Start()
    {
        base.Start();
        if ( _missilePods.Count == 0 )
        {
            UpdateActiveMissilePodsList();
        }
        SetReloadSpeed( ReloadSpeed );
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        foreach ( MissilePod pod in _missilePods )
        {
            if ( pod.isActiveAndEnabled )
            {
                pod.MissilePodFixedUpdate();
            }
        }

        if ( _currentTarget != null && CheckMinimumRange( _currentTarget.transform.position ) )
        {
            RotateTowardsTarget( _currentTarget.transform.position );
            if ( CheckFiringAngle2D( _currentTarget.transform ) )
            {
                Fire();
            }
        }
        else
        {
            SelectTarget();
        }
    }

    protected override void Fire()
    {
        if ( _remainingCooldown <= 0f )
        {
            // index range check
            if ( _nextPodIndexToFire >= 0 && _nextPodIndexToFire < _missilePods.Count )
            {
                MissilePod nextPod = _missilePods[ _nextPodIndexToFire ];
                if ( nextPod.ReadyToFire )
                {
                    nextPod.LaunchMissile();
                    Projectile missile = Instantiate( _missilePrefab, nextPod.tubeEnd.position, nextPod.tubeEnd.rotation );
                    missile.Init( this, _shootingDamage, Vector3.zero, _currentTarget );
                    
                    // set index for next pod in line
                    if ( _nextPodIndexToFire == _missilePods.Count - 1 )
                    {
                        _nextPodIndexToFire = 0;
                    }
                    else
                    {
                        _nextPodIndexToFire++;
                    }
                    ResetCooldown();
                }
            }
            else
            {
                _nextPodIndexToFire = 0;
            }
        }
    }

    private void UpdateActiveMissilePodsList()
    {
        GetComponentsInChildren( false, _missilePods );
    }

    private void SetReloadSpeed( float reloadSpeed )
    {
        foreach ( MissilePod pod in _missilePods )
        {
            pod.SetReloadTime( reloadSpeed );
        }
    }



}
