using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Structure : MonoBehaviour
{
    public int level;
    public int price;
    public StructureType type;

    public virtual bool CheckPlacementValidity(TileType tileType)
    {
        switch (type)
        {
            case StructureType.Tower:
                {
                    return tileType == TileType.BuildableFlat ? true : false;
                }
            case StructureType.Trap:
                {
                    return tileType == TileType.RoughFlat ? true : false;
                }
            default:
                {
                    Debug.Log("Structure type not set");
                    return false;
                }
        }
    }

    public bool CheckPlacementValidity(GameObject surface)
    {
        throw new System.NotImplementedException();
    }

    public enum StructureType
    {
        Tower,
        Trap
    }
}
