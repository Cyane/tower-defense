using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Tower : Structure
{
    public bool gizmos;
    protected HashSet<Enemy> _enemiesInRange = new HashSet<Enemy>();

    [SerializeField] protected Transform _rotatingBase;
    public Transform RotatingBase => _rotatingBase; 
    [SerializeField] protected Transform _barrelPivot;
    [SerializeField] protected Enemy _currentTarget;
    [SerializeField] protected float _rotationSpeed; // in degrees
    [SerializeField] protected float _maxRange;
    [SerializeField] protected float _minRange;
    //[SerializeField] protected Projectile _projectile;
    [SerializeField] protected float _projectileSpeed;
    [SerializeField] protected float _shootingDamage;
    [Tooltip( "In rounds per minute." )]
    [SerializeField] protected float _rateOfFire;
    [SerializeField] protected float _maxShootingAngle;
    protected CapsuleCollider _triggerCollider;
    protected float _remainingCooldown;
    protected bool CanFire => _remainingCooldown <= 0f ? true : false;

    public int KillCount { get; private set; }
    public float DamageDealt { get; private set; }

    protected virtual void Start()
    {
        _triggerCollider = GetComponent<CapsuleCollider>();
        SetRange( _minRange, _maxRange );
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        UpdateCooldown();
    }

    protected virtual void FixedUpdate()
    {
        if ( _currentTarget )
        {
            RotateTowardsTarget( _currentTarget.transform.position );
        }
    }

    protected virtual void UpdateCooldown()
    {
        if ( _remainingCooldown > 0f )
        {
            _remainingCooldown -= Time.fixedDeltaTime;
        }
    }

    protected virtual void ResetCooldown( float leftOverDeltaTime = 0f)
    {
        _remainingCooldown = 60f / _rateOfFire - leftOverDeltaTime;
    }

    // This rotation only works on upright turrets as of now, so don't tilt them 
    protected virtual void RotateTowardsTarget( Vector3 targetPos )
    {
        Vector3 targetDirection = targetPos - _barrelPivot.position;
        Vector3 newBaseDirection = Vector3.RotateTowards(
                _rotatingBase.forward,
                new Vector3( targetDirection.x, 0f, targetDirection.z ).normalized, // ignoring the Y axis.
                _rotationSpeed * Mathf.Deg2Rad * Time.fixedDeltaTime, // the function takes radians, but degrees are easier to visualize.
                0f
            );
        _rotatingBase.rotation = Quaternion.LookRotation( newBaseDirection );
    }

    protected virtual void SelectTarget()
    {
        //just in case an enemy escapes the list removal
        //_enemiesInRange.RemoveWhere( enemy => enemy == null );
        if ( _enemiesInRange.Count > 0 )
        {
            // select the enemy that'll reach it's destination the fastest based on AI agent path distance * speed 
            Enemy closestEnemy = null;
            foreach ( Enemy enemy in _enemiesInRange )
            {
                if ( CheckMinimumRange( enemy.transform.position ) )
                {
                    if ( closestEnemy == null || enemy.TimeUntilDestination < closestEnemy.TimeUntilDestination )
                    {
                        closestEnemy = enemy;
                    }
                }
            }
            _currentTarget = closestEnemy;

        }
        // Loop for List<Enemy> instead of HashSet
        /*
        for (int i = _enemiesInRange.Count - 1; i >= 0; i--)
        {
            if (_enemiesInRange[i] == null)
            {
                _enemiesInRange.RemoveAt(i);
                continue;
            }
            if (closestEnemy == null || _enemiesInRange[i].TimeUntilDestination < closestEnemy.TimeUntilDestination)
            {
                closestEnemy = _enemiesInRange[i];
            }
        }
        _currentTarget = closestEnemy;
        }
        */

    }

    protected virtual void Fire()
    {

    }

    public void RegisterKill()
    {
        KillCount++;
    }

    public void RegisterDamageDealt( float damage )
    {
        DamageDealt += damage;
    }

    protected void SetRange( float minRange, float maxRange )
    {
        _triggerCollider.radius = maxRange + 0.5f;
        _maxRange = maxRange;
        _minRange = minRange;
    }

    /// <summary>Discards the Y axis from the calculation</summary>
    protected bool CheckFiringAngle2D( Transform targetPos )
    {
        Vector3 direction = targetPos.position - _barrelPivot.position;
        if ( Vector2.Angle( new Vector2( _barrelPivot.forward.x, _barrelPivot.forward.z ), new Vector2( direction.x, direction.z ) ) < _maxShootingAngle )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected bool CheckFiringAngle3D( Transform targetPos )
    {
        Vector3 direction = targetPos.position - _barrelPivot.position;
        if ( Vector3.Angle( _barrelPivot.forward, direction ) < _maxShootingAngle )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected virtual bool CheckMinimumRange( Vector3 otherPosition )
    {
        if( _minRange > 0.5f ) // Don't check if there's no minimum range
        {
            // I read that using SqrMagnitude() is cheaper than Magnitude()
            return Vector3.SqrMagnitude( transform.position - otherPosition ) > Mathf.Pow( _minRange, 2 );
        }
        else
        {
            return true;
        }
    }

    private void RemoveFromTargets( Enemy enemy )
    {
        if ( _currentTarget == enemy )
        {
            _currentTarget = null;
        }
        _enemiesInRange.Remove( enemy );
        enemy.Death -= RemoveFromTargets;
    }

    protected void OnTriggerEnter( Collider other )
    {
        if ( other.gameObject.TryGetComponent<Enemy>( out Enemy enemy ) )
        {
            enemy.Death += RemoveFromTargets;
            _enemiesInRange.Add( enemy );
        }
        Debug.Log( _enemiesInRange.Count );
    }

    protected void OnTriggerExit( Collider other )
    {
        if ( other.gameObject.TryGetComponent<Enemy>( out Enemy enemy ) )
        {
            RemoveFromTargets( enemy );
        }
        Debug.Log( _enemiesInRange.Count );

    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color( 0f, 0f, 1, 0.25f );
        Gizmos.DrawWireSphere( transform.position, _maxRange + 0.5f );
    }
}
