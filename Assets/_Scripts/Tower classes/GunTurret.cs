using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CapsuleCollider))]
public class GunTurret : Tower
{
    [SerializeField] private Transform _turretBarrelEnd;
    [SerializeField] private GameObject _turretLaserGuide;
    [SerializeField] protected Projectile _projectile;
    // TODO: make laserpointer a separate prefab and class
    [SerializeField] private LineRenderer laserGuideRenderer;
    [SerializeField] private bool laserGuideEnabled;
    [SerializeField] private Color laserGuideColor;
    

    // Start is called before the third frame update
    protected override void Start()
    {
        base.Start();
        ResetGradient();
    }

    // Update is called once per full moon
    protected override void Update()
    {
        base.Update();
        UpdateTarget();
        ShootTarget();
        UpdateLaserGuide();
    }

    void UpdateTarget()
    {
        if ( _currentTarget == null && _enemiesInRange.Count > 0 )
        {
            SelectTarget();
        }
    }

    void ShootTarget()
    {
        if (CanFire && _currentTarget != null && CheckFiringAngle3D( _currentTarget.transform ) )
        {
            ShootTarget( _currentTarget.transform );
        }
    }

    // TODO: Refactor this ugly af function
    protected override void RotateTowardsTarget(Vector3 targetPos)
    {
        base.RotateTowardsTarget(targetPos);
        Vector3 targetDirection = targetPos - _barrelPivot.position;
        
        // Setting the rotation of the barrel of the turret, along the local X axis
        Vector3 barrelUpwardAngleDirection = new Vector3(
                0f, // X axis is disregarded
                Mathf.Max(0f, targetDirection.y), // returns the highest of the two, ensuring that Y is never below 0f.
                Vector2.Distance(new Vector2(_barrelPivot.position.x, _barrelPivot.position.z), new Vector2(targetPos.x, targetPos.z))
            ).normalized;

        // This rotation is just for the X axis of the barrel, so localRotation makes more sense
        _barrelPivot.localRotation = Quaternion.RotateTowards(
                _barrelPivot.localRotation,
                Quaternion.LookRotation(barrelUpwardAngleDirection, Vector3.up),
                _rotationSpeed
                );
    }

    void ShootTarget(Transform target)
    {
        Projectile bullet = Instantiate(_projectile, _turretBarrelEnd.position, _turretBarrelEnd.rotation);
        bullet.Init(this, _shootingDamage, _barrelPivot.forward * _projectileSpeed, _currentTarget );
        ResetCooldown();
    }

    void UpdateLaserGuide()
    {
        laserGuideRenderer.SetPosition(0, _turretLaserGuide.transform.position);
        RaycastHit hit;
        if (Physics.Raycast(_turretLaserGuide.transform.position, _turretLaserGuide.transform.up, out hit, _maxRange))
        {
            // position
            laserGuideRenderer.SetPosition(1, hit.point);

            // color gradient 
            Gradient newGradient = new Gradient();
            float newAlphaValue = Mathf.Lerp(laserGuideColor.a, 0f,  Vector3.Distance(hit.point, _barrelPivot.position) / _maxRange);
            newGradient.SetKeys(
                new GradientColorKey[] { new GradientColorKey(laserGuideColor, 0.0f), new GradientColorKey(laserGuideColor, 1.0f) },
                new GradientAlphaKey[] { new GradientAlphaKey(laserGuideColor.a, 0.0f), new GradientAlphaKey(newAlphaValue, 1.0f) });

            laserGuideRenderer.colorGradient = newGradient;
        }
        else
        {
            // position
            laserGuideRenderer.SetPosition(1, _turretLaserGuide.transform.position + _turretLaserGuide.transform.up * _maxRange);

            // color gradient reset
            if(laserGuideRenderer.colorGradient.alphaKeys[laserGuideRenderer.colorGradient.alphaKeys.Length - 1].alpha > 0f)
            {
                ResetGradient();
            }
        }
    }

    private void ResetGradient()
    {
        Gradient newGradient = new Gradient();
        newGradient.SetKeys(
            new GradientColorKey[] { new GradientColorKey(laserGuideColor, 0.0f), new GradientColorKey(laserGuideColor, 1.0f) },
            new GradientAlphaKey[] { new GradientAlphaKey(laserGuideColor.a, 0.0f), new GradientAlphaKey(0f, 1.0f) }
            );
        laserGuideRenderer.colorGradient = newGradient;
    }
}
